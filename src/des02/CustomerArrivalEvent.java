/**
 * 
 */
package des02;

import simulation.lib.Event;

/**
 * Class implementing a customer arrival event
 * @author David Stezenbach
 * @author Michael Hoefling
 */
public class CustomerArrivalEvent extends Event {
	private DES02 DES;
	/**
	 * Basic constructor
	 * @param eventTime event time
	 */
	public CustomerArrivalEvent(DES02 des, long eventTime) {
        super(eventTime);
		this.DES = des;

	}

	/* (non-Javadoc)
	 * @see simulator.lib.Event#process()
	 */
	@Override
	public void process() {

        long now = getTime();
        System.out.println("C-" + now);
        //new Customer, arrivaltime = getTime()

        DES.simulator.pushNewEvent(new CustomerArrivalEvent(DES, DES.simulator.getSimTime()+DES.params.interArrivalTime));
		if(DES.state.serverBusy) {
			DES.state.queueSize++;
		} else {
			DES.simulator.pushNewEvent(new ServiceCompletionEvent(DES, DES.simulator.getSimTime()+DES.params.serviceTime));
			DES.state.serverBusy = true;
		}

        DES.handleInCustomer(new Customer(now));

		// update statistics
		DES.stats.minQS = DES.state.queueSize < DES.stats.minQS ? DES.state.queueSize : DES.stats.minQS;
		DES.stats.maxQS = DES.state.queueSize > DES.stats.maxQS ? DES.state.queueSize : DES.stats.maxQS;
	}

}
