package des02;

/**
 * Created by bottersb on 24.10.2015.
 */

import simulation.lib.Simulator;
import simulation.lib.SortableQueue;
import simulation.lib.counter.ContinuousTimeCounter;
import simulation.lib.counter.DiscreteTimeCounter;
import simulation.lib.histogram.ContinuousTimeHistogram;
import simulation.lib.histogram.DiscreteTimeHistogram;

/**
 * The simulation to solve problem 1
 *
 * @author David Stezenbach
 * @author Michael Hoefling
 */
public class DES02 {
    public SimulationParametersDES02 params;
    public SimulationStatisticsDES02 stats;
    public SimulationStateDES02 state;
    public Simulator simulator;

    public SortableQueue customers;
    public DiscreteTimeCounter dtc_wait;
    public DiscreteTimeHistogram dth_wait;
    public DiscreteTimeCounter dtc_sTime;
    public DiscreteTimeHistogram dth_sTime;

    public ContinuousTimeCounter ctc_occupancy;
    public ContinuousTimeHistogram cth_occupancy;
    public ContinuousTimeCounter ctc_sUsage;
    public ContinuousTimeHistogram cth_sUsage;


    /**
     * @param args interArrivalTime serviceTime simulationDuration
     */
    public static void main(String[] args) {
		/*
		 * create simulation object
		 */
        DES02 sim = new DES02(args);
		/*
		 * run simulation
		 */
        sim.start();
		/*
		 * print out report
		 */
        sim.report();
    }

    public DES02(String[] args) {
        int simFactor = 1000;
        simulator = new Simulator();
        simulator.setSimTimeInRealTime(simFactor);

        params = new SimulationParametersDES02(this, args);
        stats = new SimulationStatisticsDES02();
        state = new SimulationStateDES02(stats);

        customers = new SortableQueue();

        dtc_wait = new DiscreteTimeCounter("Waiting Time");
        dth_wait = new DiscreteTimeHistogram("Waiting Time", 1000, 0, 1000000);
        dtc_sTime = new DiscreteTimeCounter("Service Time");
        dth_sTime = new DiscreteTimeHistogram("Service Time", 1000, 0, 1000000);

        ctc_occupancy = new ContinuousTimeCounter("Queue Occupancy", simulator);
        cth_occupancy = new ContinuousTimeHistogram("Queue Occupancy",1000,0,1000000, simulator);
        ctc_sUsage = new ContinuousTimeCounter("Server Usage", simulator);
        cth_sUsage = new ContinuousTimeHistogram("Queue Usage",1000,0,1000000, simulator);

        dtc_wait.reset();
        dth_wait.reset();
        dtc_sTime.reset();
        dth_sTime.reset();

        ctc_occupancy.reset();
        cth_occupancy.reset();
        ctc_sUsage.reset();
        cth_sUsage.reset();

        simulator.pushNewEvent(new des02.CustomerArrivalEvent(this, 0));
        simulator.pushNewEvent(new SimulationTerminationEvent(this, params.simulationDuration));
    }

    public void report() {
        params.report();
        stats.report();

        dtc_wait.report();
        dth_wait.report();
        dtc_sTime.report();
        dth_sTime.report();

        ctc_occupancy.report();
        cth_occupancy.report();
        ctc_sUsage.report();
        cth_sUsage.report();
    }

    public void start() {
        simulator.run();
    }

    public void handleInCustomer(Customer customer) {
        customers.pushNewElement(customer);
        int queueSize = customers.size();
        ctc_occupancy.count(queueSize);
        cth_occupancy.count(queueSize);

    }

    public void handleOutCustomer(long serviceCompletionTime) {
        Customer c = (Customer) customers.popNextElement();
        c.serviceCompletion = serviceCompletionTime;
        c.serviceInitiation = serviceCompletionTime - params.serviceTime;

        double waitingTime = c.serviceInitiation-c.arrivalTime;
        dtc_wait.count(waitingTime);
        dth_wait.count(waitingTime);

        double serviveTime = c.serviceCompletion - c.serviceInitiation;
        dtc_sTime.count(serviveTime);
        dth_sTime.count(serviveTime);

        int queueSize = customers.size();
        ctc_occupancy.count(queueSize);
        cth_occupancy.count(queueSize);

        double serverState = state.serverBusy ? 1 : 0;
        ctc_sUsage.count(serverState);
        cth_sUsage.count(serverState);

    }


}
