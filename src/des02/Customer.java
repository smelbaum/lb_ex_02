package des02;

import simulation.lib.SortableQueueItem;

/**
 * Created by bottersb on 24.10.2015.
 */
public class Customer extends SortableQueueItem {

    long arrivalTime, serviceInitiation, serviceCompletion;

    public Customer(long arrivalTime) {
        super();
        this.arrivalTime = arrivalTime;
    }

    @Override
    public int compareTo(SortableQueueItem o) {
        if (o instanceof Customer) {
            Customer c = (Customer) o;
            if(equals(c)) return 0;
            if(arrivalTime < c.arrivalTime) return -1;
            if(arrivalTime > c.arrivalTime) return 1;
        }
        return 1;
    }
}
