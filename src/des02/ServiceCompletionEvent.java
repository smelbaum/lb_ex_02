/**
 * 
 */
package des02;

import simulation.lib.Event;

/**
 * Class implementing a service completion event
 * @author David Stezenbach
 * @author Michael Hoefling
 */
public class ServiceCompletionEvent extends Event {
	private DES02 DES;
    public int nr;
	/**
	 * Basic constructor
	 * @param eventTime event time
	 */
	public ServiceCompletionEvent(DES02 des, long eventTime) {
		super(eventTime);
		this.DES = des;
	}

	/* (non-Javadoc)
	 * @see simulator.lib.Event#process()
	 */
	@Override
	public void process() {

		System.out.println("S-" + getTime());

        if(DES.state.queueSize > 0) {
			DES.simulator.pushNewEvent(new ServiceCompletionEvent(DES, DES.simulator.getSimTime()+DES.params.serviceTime));
			DES.state.queueSize--;
		} else {
			DES.state.serverBusy = false;
		}

        DES.handleOutCustomer(getTime());

        // update statistics
		DES.stats.minQS = DES.state.queueSize < DES.stats.minQS ? DES.state.queueSize : DES.stats.minQS;
		DES.stats.maxQS = DES.state.queueSize > DES.stats.maxQS ? DES.state.queueSize : DES.stats.maxQS;
	}

}
