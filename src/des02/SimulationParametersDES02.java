/**
 * 
 */
package des02;


/**
 * @author David Stezenbach
 * @author Michael Hoefling
 *
 */
public class SimulationParametersDES02 {
	public long simulationDuration;
	public long serviceTime;
	public long interArrivalTime;
	private DES02 DES = null;

	/**
	 * The basic constructor
	 *
	 */
	public SimulationParametersDES02(DES02 des, String[] args) {
		super();
		this.DES = des;
		parseCommandLine(args);
	}	
	
	public void parseCommandLine(String[] args)
			throws NumberFormatException {
		if (args.length !=3) {
			System.err.println("Missing Arguments!! \njava des01/DES01 E[interArrivalTime] E[serviceTime] simulationDuration (all doubles)");
			System.exit(0);
		}
		else {
			interArrivalTime = DES.simulator.realTimeToSimTime(Double.parseDouble(args[0]));
			serviceTime = DES.simulator.realTimeToSimTime(Double.parseDouble(args[1]));
			simulationDuration = DES.simulator.realTimeToSimTime(Double.parseDouble(args[2]));			
		}
	}

	public void report() {
		System.out.println(
				"E[interArrivalTime] = " + DES.simulator.simTimeToRealTime(interArrivalTime) + "\n" +
				"E[serviceTime] = " + DES.simulator.simTimeToRealTime(serviceTime) + "\n" +
				"simulation duration: " + DES.simulator.simTimeToRealTime(simulationDuration) + "\n");
	}	
}
