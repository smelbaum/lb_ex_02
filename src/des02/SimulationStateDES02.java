/**
 * 
 */
package des02;

/**
 * @author David Stezenbach
 *
 */
public class SimulationStateDES02 {
	public boolean serverBusy;
	public long queueSize;
	
	public SimulationStateDES02(SimulationStatisticsDES02 stats) {
		queueSize = 0;
		serverBusy = false;
	}	
}
