/**
 * 
 */
package des01;


/**
 * @author David Stezenbach
 * @author Michael Hoefling
 *
 */
public class SimulationParametersDES01 {
	public long simulationDuration;
	public long serviceTime;
	public long interArrivalTime;
	private DES01 DES = null;
	
	/**
	 * The basic constructor
	 * @param interArrivalTime interarrival time between customers
	 * @param serviceTime service time per customer
	 * @param simulationDuration total simulation duration
	 * 
	 */
	public SimulationParametersDES01(DES01 des, String[] args) {
		super();
		this.DES = des;
		parseCommandLine(args);
	}	
	
	public void parseCommandLine(String[] args)
			throws NumberFormatException {
		if (args.length !=3) {
			System.err.println("Missing Arguments!! \njava des01/DES01 E[interArrivalTime] E[serviceTime] simulationDuration (all doubles)");
			System.exit(0);
		}
		else {
			interArrivalTime = DES.simulator.realTimeToSimTime(Double.parseDouble(args[0]));
			serviceTime = DES.simulator.realTimeToSimTime(Double.parseDouble(args[1]));
			simulationDuration = DES.simulator.realTimeToSimTime(Double.parseDouble(args[2]));			
		}
	}

	public void report() {
		System.out.println(
				"E[interArrivalTime] = " + DES.simulator.simTimeToRealTime(interArrivalTime) + "\n" +
				"E[serviceTime] = " + DES.simulator.simTimeToRealTime(serviceTime) + "\n" +
				"simulation duration: " + DES.simulator.simTimeToRealTime(simulationDuration) + "\n");
	}	
}
