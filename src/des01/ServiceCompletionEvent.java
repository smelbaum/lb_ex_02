/**
 * 
 */
package des01;

import simulation.lib.Event;

/**
 * Class implementing a service completion event
 * @author David Stezenbach
 * @author Michael Hoefling
 */
public class ServiceCompletionEvent extends Event {
	private DES01 DES;
	/**
	 * Basic constructor
	 * @param eventTime event time
	 */
	public ServiceCompletionEvent(DES01 des, long eventTime) {
		super(eventTime);
		this.DES = des;
	}

	/* (non-Javadoc)
	 * @see simulator.lib.Event#process()
	 */
	@Override
	public void process() {
		System.out.println("S-"+getTime());
		if(DES.state.queueSize > 0) {
			DES.simulator.pushNewEvent(new ServiceCompletionEvent(DES, DES.simulator.getSimTime()+DES.params.serviceTime));
			DES.state.queueSize--;
		} else {
			DES.state.serverBusy = false;
		}
		
		// update statistics
		DES.stats.minQS = DES.state.queueSize < DES.stats.minQS ? DES.state.queueSize : DES.stats.minQS;
		DES.stats.maxQS = DES.state.queueSize > DES.stats.maxQS ? DES.state.queueSize : DES.stats.maxQS;
	}

}
