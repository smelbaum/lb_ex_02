/**
 * 
 */
package des01;

/**
 * @author David Stezenbach
 *
 */
public class SimulationStateDES01 {
	public boolean serverBusy;
	public long queueSize;
	
	public SimulationStateDES01(SimulationStatisticsDES01 stats) {
		queueSize = 0;
		serverBusy = false;
	}	
}
