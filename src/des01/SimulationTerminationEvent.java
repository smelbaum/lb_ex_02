/**
 * 
 */
package des01;

import simulation.lib.Event;


/**
 * This events terminates the simulation
 * 
 * @author David Stezenbach
 * @author Michael Hoefling
 */
public class SimulationTerminationEvent extends Event {
	private DES01 DES = null;
	
	/**
	 * The basic constructor
	 * @param des the simulator
	 * @param eventTime the event time
	 */
	public SimulationTerminationEvent(DES01 des, long eventTime) {
		super(eventTime);
		this.DES = des;
	}

	/* (non-Javadoc)
	 * @see simulator.lib.Event#process()
	 */
	@Override
	public void process() {
		DES.simulator.stop();
	}

}
