package des01;

import simulation.lib.Simulator;

/**
 * The simulation to solve problem 1
 * 
 * @author David Stezenbach
 * @author Michael Hoefling
 */
public class DES01 {
	public SimulationParametersDES01 params;
	public SimulationStatisticsDES01 stats;
	public SimulationStateDES01 state;
	public Simulator simulator;
	
	/**
	 * @param args interArrivalTime serviceTime simulationDuration
	 */
	public static void main(String[] args) {
		/*
		 * create simulation object
		 */
		DES01 sim = new DES01(args);
		/*
		 * run simulation
		 */
		sim.start();
		/*
		 * print out report
		 */
		sim.report();
	}	

	public DES01(String[] args) {
		simulator = new Simulator();
		simulator.setSimTimeInRealTime(1000);
		
		params = new SimulationParametersDES01(this, args);
		stats = new SimulationStatisticsDES01();
		state = new SimulationStateDES01(stats);
		
		simulator.pushNewEvent(new CustomerArrivalEvent(this, 0));
		simulator.pushNewEvent(new SimulationTerminationEvent(this, params.simulationDuration));		
	}

	public void report() {
		params.report();
		stats.report();
	}
	
	public void start() {
		simulator.run();
	}
}
