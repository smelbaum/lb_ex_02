package des01;

public class SimulationStatisticsDES01 {
	public long maxQS;
	public long minQS;
	
	public SimulationStatisticsDES01() {
		maxQS = Long.MIN_VALUE;
		minQS = Long.MAX_VALUE;
	}
	
	public void report() {
		System.out.println(
				"minimum queue size: " + minQS + "\n" +
				"maximum queue size: " + maxQS);
	}	
}
