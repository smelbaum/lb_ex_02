package simulation.lib;

/**
 * Abstract class for events
 * All events have to extend this class
 * 
 * @author David Stezenbach
 * @author Michael Hoefling
 */
public abstract class Event extends SortableQueueItem {
	private long eventTime;
	
	/**
	 * Creates a new event
	 * @param sim the simulator
	 * @param eventTime event time
	 */
	public Event(long eventTime) {
		this.eventTime = eventTime;
	}
	
	/**
	 * Starts the event
	 * @throws Exception 
	 */
	public abstract void process();	

	/**
	 * Returns the event time
	 * @return the event time
	 */
	public long getTime() {
		return eventTime;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SortableQueueItem o) {
		if (o instanceof Event) {
			Event e = (Event) o;
			if(equals(o)) return 0;
			if(eventTime < e.eventTime) return -1;
			if(eventTime > e.eventTime) return 1;
		}
		return 1;
	}
	
}
