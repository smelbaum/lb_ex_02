/**
 * 
 */
package simulation.lib.randVars.continous;

import simulation.lib.randVars.RandVar;
import simulation.lib.rng.RNG;

/**
 * @author David Stezenbach
 *
 */
public class Normal extends RandVar {
	private boolean cacheEmpty;
	private double mu;
	private double sigma2;
	private double cache;

	public Normal() {
		this(0, 1);
	}
	
	public Normal(double mean, double var) {
		super();
		setMeanAndStdDeviation(mean, Math.sqrt(var));
	}
	
	public Normal(RNG rng) {
		this(rng, 0, 1);
	}
	
	public Normal(RNG rng, double mean, double var) {
		super(rng);
		setMeanAndStdDeviation(mean, Math.sqrt(var));		
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getMean()
	 */
	@Override
	public double getMean() {
		return mu;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getRV()
	 */
	@Override
	public double getRV() {
        if (!cacheEmpty) {
            cacheEmpty = true;
            return cache;
        }else{
            double v1 = 2 * rng.rnd() - 1;
            double v2 = 2 * rng.rnd() - 1;
            double w;
    
            while ((w = v1 * v1 + v2 * v2) > 1) {
                v1 = 2 * rng.rnd() - 1;
                v2 = 2 * rng.rnd() - 1;
            }
    
            double y = Math.sqrt(-2 * Math.log(w) / w );
            cache = mu + Math.sqrt(sigma2) * v2 * y;
            cacheEmpty = false;
            return mu + Math.sqrt(sigma2) * v1 * y;
        }
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getType()
	 */
	@Override
	public String getType() {
		return "Normal";
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getVariance()
	 */
	@Override
	public double getVariance() {
		return sigma2;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMean(double)
	 */
	@Override
	public void setMean(double m) {
		setMeanAndStdDeviation(m,getStdDeviation());
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMeanAndStdDeviation(double, double)
	 */
	@Override
	public void setMeanAndStdDeviation(double m, double s) {
        cacheEmpty = true;
        mu = m;    
        sigma2 = s * s;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setStdDeviation(double)
	 */
	@Override
	public void setStdDeviation(double s) {
		setMeanAndStdDeviation(getMean(),s);
	}

	@Override
	public String toString() {
		return super.toString() + 
			"\tparameters:\n" +
			"\t\tmu: " + mu + "\n" +
			"\t\tsigma2: " + sigma2 + "\n";
	}	
	
}
