/**
 * 
 */
package simulation.lib.randVars.continous;

import simulation.lib.randVars.RandVar;
import simulation.lib.rng.RNG;

/**
 * @author David Stezenbach
 * @author Michael Hoefling
 */
public class ErlangK extends RandVar {
	private double lambda;
	private int k;
	
	public ErlangK() {
		this(1, 1);
	}
	
	public ErlangK(double mean, int k) {
		super();
		lambda = 1 / mean;
		this.k = k;
	}
	
	public ErlangK(RNG rng) {
		this(rng, 1, 1);
	}
	
	public ErlangK(RNG rng, double mean, int k) {
		super(rng);
		lambda = 1 / mean;
		this.k = k;
	}
	
	
	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getMean()
	 */
	@Override
	public double getMean() {
		return k/lambda;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getRV()
	 */
	@Override
	public double getRV() {
        double s = 1.0;
        for (int i=0; i < k; i++){
            s = s * rng.rnd();
        }
        return (-Math.log(s)/k)*(1/lambda);
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getType()
	 */
	@Override
	public String getType() {
		return "Erlang-k";
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getVariance()
	 */
	@Override
	public double getVariance() {
		return k / (lambda * lambda);
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMean(double)
	 */
	@Override
	public void setMean(double m) {
        if(m <= 0)
            throw new IllegalArgumentException("mean must not be lower equals 0");
	    lambda = k / m;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMeanAndStdDeviation(double, double)
	 */
	@Override
	public void setMeanAndStdDeviation(double m, double s) {
        throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setStdDeviation(double)
	 */
	@Override
	public void setStdDeviation(double s) {
	    k = (int) (s * s * lambda * lambda);
	}

	@Override
	public String toString() {
		return super.toString() + 
			"\tparameters:\n" +
			"\t\tlambda: " + lambda + "\n" +
			"\t\tk: " + k + "\n";
	}		
}
