/**
 * 
 */
package simulation.lib.randVars.continous;

import simulation.lib.randVars.RandVar;
import simulation.lib.rng.RNG;
import static java.lang.Math.*;

/**
 * @author Alfons Martin
 *
 */
public class HyperExponential extends RandVar {

	private double lambda1;
	private double lambda2;
	private double p1;
	private double p2;
	private double Mean;
	
	public HyperExponential() {
		this(1,2);
	}

	public HyperExponential(double mean, double Cvar) {
		super();
		double helper=((Cvar*Cvar)-1)/((Cvar*Cvar)+1);
		helper=sqrt(helper);
		lambda1 = 1 / mean*(1+helper);
		lambda2 = 1 / mean*(1-helper);
		p2=lambda2/(lambda1+lambda2);
		p1=1-p2;
	}
	
	public HyperExponential(RNG rng) {
		this(rng, 1, 2);
	}
	
	public HyperExponential(RNG rng, double mean, double Cvar) {
		super(rng);
		double helper=((Cvar*Cvar)-1)/((Cvar*Cvar)+1);
		helper=sqrt(helper);
		lambda1 = 1 / mean*(1+helper);
		lambda2 = 1 / mean*(1-helper);
		p2=lambda2/(lambda1+lambda2);
		p1=1-p2;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getMean()
	 */
	@Override
	public double getMean() {
		return (p1/lambda1+p2/lambda2);
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getRV()
	 */
	@Override
	public double getRV() {
		double u1=rng.rnd();
		if(p1<=u1){
			return -(Math.log(rng.rnd()) * (1/lambda1));
		}else{
			return -(Math.log(rng.rnd()) * (1/lambda2));
		}
		
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getType()
	 */
	@Override
	public String getType() {
		return "HyperExponential";
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getVariance()
	 */
	@Override
	public double getVariance() {
		return sqrt((2*((p1/(lambda1*lambda1))+(p2/(lambda2*lambda2))))/(((p1/lambda1)+(p2/lambda2))*(p1/lambda1)+(p2/lambda2)));
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMean(double)
	 */
	@Override
	public void setMean(double m) {
		if(m <= 0)
			throw new IllegalArgumentException("mean must not be lower equals 0");
        Mean =  m;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMeanAndStdDeviation(double, double)
	 */
	@Override
	public void setMeanAndStdDeviation(double m, double s) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setStdDeviation(double)
	 */
	@Override
	public void setStdDeviation(double s) {
		throw new UnsupportedOperationException();
	}
	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setCvar(double)
	 */
	@Override
	public void setCvar(double c) {
		double helper;
		if(Mean != 0){
			helper=((c*c)-1)/((c*c)+1);
			helper=sqrt(helper);
			lambda1 = 1 / Mean*(1+helper);
			lambda2 = 1 / Mean*(1-helper);
		}else
			throw new IllegalArgumentException("mean == 0 -> cvar can not be set");
	}
	
	@Override
	public String toString() {
		return super.toString() + 
			"\tparameters:\n" +
			"\t\tlambda1: " + lambda1 + "\n"+
			"\t\tlambda2: " + lambda2 + "\n"+
			"\t\tp1: " + p1 + "\n"+
			"\t\tp2: " + p2 + "\n";
	}	
	
}
