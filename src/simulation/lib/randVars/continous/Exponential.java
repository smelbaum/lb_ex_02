/**
 * 
 */
package simulation.lib.randVars.continous;

import simulation.lib.randVars.RandVar;
import simulation.lib.rng.RNG;

/**
 * @author David Stezenbach
 *
 */
public class Exponential extends RandVar {

	private double lambda;

	public Exponential() {
		this(1);
	}

	public Exponential(double mean) {
		super();
		lambda = 1 / mean;
	}
	
	public Exponential(RNG rng) {
		this(rng, 1);
	}
	
	public Exponential(RNG rng, double mean) {
		super(rng);
		lambda = 1 / mean;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getMean()
	 */
	@Override
	public double getMean() {
		return 1 / lambda;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getRV()
	 */
	@Override
	public double getRV() {
		return -(Math.log(rng.rnd()) * (1/lambda));
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getType()
	 */
	@Override
	public String getType() {
		return "Exponential";
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getVariance()
	 */
	@Override
	public double getVariance() {
		return 1 / (lambda*lambda);
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMean(double)
	 */
	@Override
	public void setMean(double m) {
		if(m <= 0)
			throw new IllegalArgumentException("mean must not be lower equals 0");
        lambda = (1 / m);
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMeanAndStdDeviation(double, double)
	 */
	@Override
	public void setMeanAndStdDeviation(double m, double s) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setStdDeviation(double)
	 */
	@Override
	public void setStdDeviation(double s) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public String toString() {
		return super.toString() + 
			"\tparameters:\n" +
			"\t\tlambda: " + lambda + "\n";
	}	
	
}
