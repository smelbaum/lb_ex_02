/**
 * 
 */
package simulation.lib.randVars.continous;

import simulation.lib.randVars.RandVar;
import simulation.lib.rng.RNG;

/**
 * @author David Stezenbach
 *
 */
public class Uniform extends RandVar {
	private double lowerBound;
	private double upperBound;
	
	public Uniform() {
		this(0, 1);
	}
	
	public Uniform(double lowerBound, double upperBound) {
		super();
		setBounds(lowerBound, upperBound);
	}
	
	public Uniform(RNG rng) {
		this(rng,0,1);
	}
	
	public Uniform(RNG rng, double lowerBound, double upperBound) {
		super(rng);
		setBounds(lowerBound, upperBound);
	}
	
	public void setBounds(double lowerBound, double upperBound) {
		if(lowerBound>=upperBound)
			throw new IllegalArgumentException("uniform distribution: lower bound must not be greater then upper bound");
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}
	
	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getMean()
	 */
	@Override
	public double getMean() {
		return (upperBound+lowerBound)/2;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getRV()
	 */
	@Override
	public double getRV() {
		return lowerBound + rng.rnd() * (upperBound - lowerBound);
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getType()
	 */
	@Override
	public String getType() {
		return "Uniform";
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#getVariance()
	 */
	@Override
	public double getVariance() {
		return Math.pow((upperBound-lowerBound),2)/12;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMean(double)
	 */
	@Override
	public void setMean(double m) {
		double tmp=m-getMean();
		lowerBound+=tmp;
		upperBound+=tmp;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setMeanAndStdDeviation(double, double)
	 */
	@Override
	public void setMeanAndStdDeviation(double m, double s) {
		lowerBound = m-Math.sqrt(3)*s;
		upperBound = m+Math.sqrt(3)*s;
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RandVar#setStdDeviation(double)
	 */
	@Override
	public void setStdDeviation(double s) {
		setMeanAndStdDeviation(getMean(),s);
	}
	
	@Override
	public String toString() {
		return super.toString() + 
			"\tparameters:\n" +
			"\t\tlowerBound: " + lowerBound + "\n" +
			"\t\tupperBound: " + upperBound + "\n";
	}
	
}
