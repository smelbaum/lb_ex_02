/**
 * 
 */
package simulation.lib.randVars;

import simulation.lib.rng.RNG;
import simulation.lib.rng.StdRNG;

/**
 * @author David Stezenbach
 *
 */
public abstract class RandVar {
	
	protected RNG rng;

	public RandVar() {
		this(new StdRNG());
	}
	
	public RandVar(RNG rng) {
		this.rng = rng;
	}
	
	public abstract double getRV();

	public long getLongRV() {
		return (long) Math.ceil(getRV());
	}
	
	public abstract double getMean();
	
	public abstract double getVariance();
	
	public double getStdDeviation() {
		return Math.sqrt(getVariance());
	}
	
	public double getCvar() {
		if(getMean() == 0)
			return getStdDeviation()==0?0:Double.POSITIVE_INFINITY;
		else
			return getStdDeviation()/getMean();
	}
	
	public void setVariance(double v) {
		if(v >= 0)
			setStdDeviation(Math.sqrt(v));
		else
			throw new IllegalArgumentException("variance = " + v + " must be positive");
	}
	
	public void setCvar(double c) {
		if(getMean() != 0)
			setStdDeviation(getMean()*c);
		else
			throw new IllegalArgumentException("mean == 0 -> cvar can not be set");
	}
	
	public abstract void setMean(double m);
	
	public abstract void setStdDeviation(double s);
	
	public abstract void setMeanAndStdDeviation(double m, double s);
	
	public void setMeanAndCvar(double m, double c) {
		setMeanAndStdDeviation(m, m*c);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return 	"\nanalytical propterties: " + getType() + "\n" +
				"\tmean: " + getMean() + "\n" +
				"\tcvar: " + getCvar() + "\n" +	
				"\tstdandard deviation: " + getStdDeviation() + "\n" +
				"\tvariance: " + getVariance() +"\n";
	}

	public abstract String getType();
}
