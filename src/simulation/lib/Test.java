package simulation.lib;

import simulation.lib.counter.DiscreteTimeConfidenceCounter;
import simulation.lib.randVars.continous.Normal;

import java.util.ArrayList;

/**
 * Created by Svenja on 02.11.2015.
 */
public class Test {

    public static void main(String[] args) {
        int mean = 10;

        int[] samples = {5, 10, 50, 100};
        double[] alpha = {0.1, 0.05};

        double[] cvar={0.25,0.5,1,2,4};
        ArrayList<Normal> normals = new ArrayList<Normal>();
        for (int i = 0; i < 5; i++) {
            normals.add(new Normal());
            normals.get(i).setMean(10);
        }
        for (int i = 0; i < cvar.length; i++) {
            normals.get(i).setCvar(cvar[i]);
        }


        ArrayList<DiscreteTimeConfidenceCounter> confidenceCounters = new ArrayList<DiscreteTimeConfidenceCounter>();
        confidenceCounters.add(new DiscreteTimeConfidenceCounter("1"));
        confidenceCounters.add(new DiscreteTimeConfidenceCounter("2"));
        confidenceCounters.add(new DiscreteTimeConfidenceCounter("3"));
        confidenceCounters.add(new DiscreteTimeConfidenceCounter("4"));
        confidenceCounters.add(new DiscreteTimeConfidenceCounter("5"));

        for (int i = 0; i < confidenceCounters.size(); i++) {
            confidenceCounters.get(i).setAlpha(0.05);
        }
        int[] counter = new int[5];


        for (int i = 0; i < samples.length; i++) {
            for (int j = 0; j < counter.length; j++) {
                counter[j] = 0;
            }
            for (int num = 0; num < 500; num++) {
                for (int j = 0; j < samples[i]; j++) {
                    for (int k = 0; k < confidenceCounters.size(); k++) {
                        confidenceCounters.get(k).count(normals.get(k).getRV());
                    }
                }
                for (int j = 0; j < counter.length; j++) {
//                    if (num==10)confidenceCounters.get(j).report();
                    if (confidenceCounters.get(j).getLowerBound() <= mean && mean <= confidenceCounters.get(j).getUpperBound())
                        counter[j]++;
                }
            }
            for (int j = 0; j < counter.length; j++) {
                System.out.println(counter[j]);
            }
            System.out.println();
        }
    }
}
