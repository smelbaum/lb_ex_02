/**
 * 
 */
package simulation.lib.histogram;


/**
 * This class implements a discrete time histogram
 * 
 * @author David Stezenbach
 *
 */
public class DiscreteTimeHistogram extends Histogram {
	private long numSamples;
	
	/**
	 * Basic constructor
	 *  
	 * @param variable the observed variable
	 * @param numIntervals number of intervals
	 * @param lowerBound the lower bound of the histogram
	 * @param upperBound the upper bound of the histogram
	 * @param sim the considered simulator
	 */	
	public DiscreteTimeHistogram(String variable, int numIntervals, double lowerBound,
			double upperBound) {
		super(variable, numIntervals, lowerBound, upperBound, "histogram type: discrete-time histogram");
	}

	/*
	 * (non-Javadoc)
	 * @see simulator.lib.histogram.Histogram#count(double)
	 */
	@Override
	public void count(double x) {
		if(getNumIntervals() > 0) {
			incrementBin(getBinNumber(x), 1);
			numSamples++;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see simulator.lib.histogram.Histogram#getNormalizingFactor()
	 */
	@Override
	protected double getNormalizingFactor() {
		return numSamples;
	}
	
	@Override
	public void reset() {
		super.reset();
		numSamples = 0;
	}
}
