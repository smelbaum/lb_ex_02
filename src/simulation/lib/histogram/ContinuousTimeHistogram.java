/**
 * 
 */
package simulation.lib.histogram;

import simulation.lib.Simulator;

/**
 * This class implements a continuous time histogram
 * 
 * @author David Stezenbach
 *
 */
public class ContinuousTimeHistogram extends Histogram {
	private double lastSampleSize;
	private long firstSampleTime;
	private long lastSampleTime;
	private Simulator sim;
	/**
	 * Basic constructor
	 *  
	 * @param variable the observed variable
	 * @param numIntervals number of intervals
	 * @param lowerBound the lower bound of the histogram
	 * @param upperBound the upper bound of the histogram
	 * @param sim the considered simulator
	 */	
	public ContinuousTimeHistogram(String variable, int numIntervals, int lowerBound,
			int upperBound, Simulator sim) {
		super(variable, numIntervals, lowerBound, upperBound, "histogram type: continuous-time histogram");
		firstSampleTime = 0;
		lastSampleTime = 0;
		lastSampleSize = 0;
		this.sim = sim;
	}

	/*
	 * (non-Javadoc)
	 * @see simulator.lib.histogram.Histogram#count(double)
	 */
	@Override
	public void count(double x) {
		if(getNumIntervals() > 0) {
			incrementBin(getBinNumber(lastSampleSize), sim.getSimTime()-lastSampleTime);
			lastSampleTime = sim.getSimTime();
		}
		lastSampleSize = x; 
	}
	
	/*
	 * (non-Javadoc)
	 * @see simulator.lib.histogram.Histogram#getNormalizingFactor()
	 */
	@Override
	protected double getNormalizingFactor() {
		return lastSampleTime-firstSampleTime;
	}
	
	@Override
	public void reset() {
		super.reset();
		firstSampleTime = sim.getSimTime();
		lastSampleTime = sim.getSimTime();
		lastSampleSize = 0;
	}
}
