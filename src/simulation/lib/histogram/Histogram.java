/**
 * 
 */
package simulation.lib.histogram;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Basic class implemented by all types of histograms
 * 
 * @author David Stezenbach
 *
 */
public abstract class Histogram {
	private double lowerBound;
	private double upperBound;
	private double delta;
	private int numIntervals;
	private double[] bins;
	private String observedVariable;
	private String histogramType;
	
	/**
	 * Basic constructor
	 *  
	 * @param variable the observed variable
	 * @param numIntervals number of intervals
	 * @param lowerBound the lower bound of the histogram
	 * @param upperBound the upper bound of the histogram
	 * @param sim the considered simulator
	 */
	public Histogram(String variable, int numIntervals, double lowerBound, double upperBound) {
		this(variable, numIntervals, lowerBound, upperBound, "histogram type: base histogram");
	}
	
	/**
	 * Internal constructor
	 * 
	 * @param variable the observed variable
	 * @param numIntervals number of intervals
	 * @param lowerBound the lower bound of the histogram
	 * @param upperBound the upper bound of the histogram
	 * @param histogramType the type of histogram
	 * @param sim the considered simulator
	 */
	protected Histogram(String variable, int numIntervals, double lowerBound, double upperBound, String histogramType) {
		this.observedVariable = variable;
		this.histogramType = histogramType;
		this.numIntervals = numIntervals;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		bins = new double[numIntervals];
		delta = (double) (upperBound-lowerBound)/numIntervals;
	}
	
	/**
	 * Counts a new sample
	 * @param x the value to count
	 */
	public abstract void count(double x);
	
	/**
	 * Returns the number of intervals
	 * @return the number ofer intervals
	 */
	public long getNumIntervals() {
		return numIntervals;
	}
	
	/**
	 * Returns the bin number the value is belonging to
	 * @param x the value
	 * @return the number of the bin
	 */
	protected int getBinNumber(double x) {
		if(x<lowerBound) return 0;
		else if(x>=upperBound) return numIntervals-1;
		else return (int) Math.floor((x-lowerBound) / delta);
	}
	
	/**
	 * Adds the given value to the selected bin
	 * @param binNumber the bin number
	 * @param x the value to add
	 */
	protected void incrementBin(int binNumber, double x) {
		bins[binNumber] += x;
	}
	
	/**
	 * Returns the current value of the selected bin
	 * @param binNumber the bin number
	 * @return the current value
	 */
	public double getBinValue(int binNumber) {
		return bins[binNumber];
	}

	/**
	 * Returns the lower bound
	 * @return the lowerBound
	 */
	public double getLowerBound() {
		return lowerBound;
	}

	/**
	 * Returns the upper bound
	 * @return the upperBound
	 */
	public double getUpperBound() {
		return upperBound;
	}

	/**
	 * Returns the delta
	 * @return the delta
	 */
	public double getDelta() {
		return delta;
	}	
	
	/**
	 * Returns the factor needed to normalize the values
	 * @return the normalizing factor
	 */
	protected abstract double getNormalizingFactor();
	
	/**
	 * Outputs the report of the histogram in csv files
	 */
	public void report() {
		try {
			File file = new File("output");
			file.mkdir();
			FileWriter histWriter = new FileWriter("output/" + observedVariable + "_hist.csv");
			FileWriter pdfWriter = new FileWriter("output/" + observedVariable + "_pdf.csv");
			FileWriter distWriter = new FileWriter("output/" + observedVariable + "_dist.csv");
			
			histWriter.append("#" + histogramType + "\n#" + observedVariable + "\n");
			pdfWriter.append("#" + histogramType + "\n#" + observedVariable + "\n");
			distWriter.append("#" + histogramType + "\n#" + observedVariable + "\n");
			
			histWriter.append("#lowerBound ; upperBound ; relative frequency\n");
			pdfWriter.append("#lowerBound ; upperBound ; probability density\n");
			distWriter.append("#(lowerBound+upperBound)/2 ; probability\n");
			
			String hist = "";
			String pdf = "";
			String dist = "";
			
			for (int i = 0; i < getNumIntervals(); i++) {
				hist += (getLowerBound()+i*getDelta()) + ";" + (getLowerBound()+(i+1)*getDelta()) + ";" + getBinValue(i)/getNormalizingFactor() + "\n";
				pdf += (getLowerBound()+i*getDelta()) + ";" + (getLowerBound()+(i+1)*getDelta()) + ";" + getBinValue(i)/getNormalizingFactor()/getDelta() + "\n";
				dist += (getLowerBound()+(i+0.5)*getDelta()) + ";" + getBinValue(i)/getNormalizingFactor() + "\n";
			}
			
			histWriter.append(hist.replace('.', ','));
			pdfWriter.append(pdf.replace('.', ','));
			distWriter.append(dist.replace('.', ','));
			
			histWriter.close();
			pdfWriter.close();
			distWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void reset() {
		bins = new double[numIntervals];
	}
}
