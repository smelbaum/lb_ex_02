package simulation.lib.counter;

/**
 * Created by Svenja on 29.10.2015.
 */
public class DiscreteTimeConfidenceCounter extends DiscreteTimeCounter {
    private double alpha = 0.01;

    public DiscreteTimeConfidenceCounter(String variable) {
        super(variable);
    }

    protected DiscreteTimeConfidenceCounter(String variable, String type, double alpha) {
        super(variable, type);
        this.alpha = alpha;
    }

    public DiscreteTimeConfidenceCounter(String variable, double alpha) {
        super(variable);
        this.alpha = alpha;
    }

    public double getZ(long numSamples) {
        double[] alpha = {0.01, 0.05, 0.1};
        double[] sam = {2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 1000000};
        double[][] student_T = {{63.657, 9.925, 5.841, 4.604, 4.032, 3.707, 3.499, 3.355, 3.25,
                2.861, 2.756, 2.704, 2.678, 2.66, 2.648, 2.639, 2.632, 2.626, 2.576},
                {12.706, 4.303, 3.182, 2.776, 2.571, 2.447, 2.365, 2.306, 2.262,
                        2.093, 2.045, 2.021, 2.009, 2, 1.994, 1.99, 1.987, 1.984, 1.96},
                {6.314, 2.92, 2.353, 2.132, 2.015, 1.943, 1.895, 1.86, 1.833,
                        1.729, 1.699, 1.684, 1.676, 1.671, 1.667, 1.664, 1.662, 1.66, 1.645}};

        for (int i = 0; i < sam.length; i++) {
            for (int j = 0; j < alpha.length; j++) {
                if (numSamples == sam[i] && this.alpha == alpha[j]) {
                    return student_T[j][i];
                }
            }
            if (numSamples == sam[i]) {
                return student_T[getRow()][i];
            }
        }
        if (numSamples > sam[18]) {
            return student_T[getRow()][18];
        }
        int i = 0;
        while (numSamples > sam[i]  && i < sam.length - 1) {
            i++;
        }
        return linearInterpol(sam[i - 1], sam[i], student_T[getRow()][i - 1], student_T[getRow()][i], numSamples);
    }

    private int getRow() {
        if (this.alpha < 0.05) {
            return 0;
        }
        if (this.alpha < 0.1) {
            return 1;
        }
        return 2;
    }

    private double linearInterpol(double nlow, double nhigh, double zlow, double zhigh, long numSamples) {
        return zlow + (zhigh - zlow) / (nhigh - nlow) * (numSamples - nlow);
    }

    public void report() {
        System.out.println("alpha: " + this.alpha + ", z: " + getZ(getNumSamples())+ ", lower bound: " + getLowerBound() + ", upper bound " + getUpperBound());
    }

    public double getBound() {
        return getZ(getNumSamples()) * Math.sqrt(getVariance() / getNumSamples());
    }

    public double getLowerBound() {
        return getMean() - getBound();
    }

    public double getUpperBound() {
        return getMean() + getBound();
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }
}
