package simulation.lib.counter;


/**
 * This class implements a discrete time counter
 * @author David Stezenbach
 *
 */
public class DiscreteTimeCounter extends Counter {

	/**
	 * Basic constructor
	 * @param variable the variable to observe
	 */
	public DiscreteTimeCounter(String variable) {
		super(variable, "counter type: discrete-time counter");
	}
	
	/**
	 * Basic constructor
	 * @param variable the variable to observe
	 */
	protected DiscreteTimeCounter(String variable, String type) {
		super(variable, type);
	}	
	
	/*
	 * (non-Javadoc)
	 * @see simulator.lib.counter.Counter#getMean()
	 */
	@Override
	public double getMean() {
		return getNumSamples()>0 ? getSumPowerOne() / getNumSamples() : 0;
	}
	
	/*
	 * (non-Javadoc)
	 * @see simulator.lib.counter.Counter#getVariance()
	 */
	@Override
	public double getVariance() {
		if(getNumSamples() >0)
			return getNumSamples()/(double)(getNumSamples()-1)*(getSumPowerTwo()/getNumSamples()-getMean()*getMean()); //Equation 2.30
		else
			return 0;
	}
	
	/*
	 * (non-Javadoc)
	 * @see simulator.lib.counter.Counter#count(double)
	 */
	@Override
	public void count(double x) {
		super.count(x);
		increaseSumPowerOne(x);
		increaseSumPowerTwo(x*x);
	}
}
