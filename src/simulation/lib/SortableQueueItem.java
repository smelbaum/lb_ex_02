package simulation.lib;

/**
 * Item for a sortable queue
 * 
 * @author David Stezenbach
 *
 */
public abstract class SortableQueueItem implements Comparable<SortableQueueItem>{
}