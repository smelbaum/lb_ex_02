/**
 * 
 */
package simulation.lib.rng;

import java.util.Random;

/**
 * @author David Stezenbach
 *
 */
public class StdRNG extends RNG {
	Random rng;
	
	public StdRNG() {
		super();
	}
	
	public StdRNG(long currentTimeMillis) {
		super(currentTimeMillis);
	}

	/*
	 * (non-Javadoc)
	 * @see simulation.lib.randVars.RNG#setSeed(long)
	 */
	@Override
	public void setSeed(long seed) {
		if(rng==null)
			rng = new Random(seed);
		else
			rng.setSeed(seed);
	}

	/* (non-Javadoc)
	 * @see simulation.lib.randVars.RNG#rnd()
	 */
	@Override
	public double rnd() {
		double tmp=0;
		/*
		 * get random number except 0 and 1
		 */
		for(;(tmp==0 || tmp==1);tmp=rng.nextDouble());
		return tmp;
	}

	@Override
	protected long longRnd() {
		throw new UnsupportedOperationException();
	}
}
