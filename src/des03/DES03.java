package des03;

import simulation.lib.counter.DiscreteTimeCounter;
import simulation.lib.randVars.continous.Exponential;
import simulation.lib.randVars.continous.HyperExponential;
import simulation.lib.randVars.continous.ErlangK;
import simulation.lib.randVars.continous.Uniform;

/**
 * Created by Svenja on 24.10.2015.
 */
public class DES03 {

    public static void main(String[] args) {
        Uniform Uniform1 = new Uniform();
        Uniform1.setMean(1);
        Uniform1.setCvar(0.1);

        Uniform Uniform2 = new Uniform();
        Uniform2.setMean(1);
        Uniform2.setCvar(1);

        Uniform Uniform3 = new Uniform();
        Uniform3.setMean(1);
        Uniform3.setCvar(2);

        Exponential Exponential1 = new Exponential();
        Exponential1.setMean(1);
      //  Exponential1.setCvar(1);

        HyperExponential h2 = new HyperExponential();
        h2.setMean(1);
        h2.setCvar(1);

        HyperExponential h3 = new HyperExponential();
        h3.setMean(1);
        h3.setCvar(2);

        DiscreteTimeCounter u1 = new DiscreteTimeCounter("u1");
        DiscreteTimeCounter u2 = new DiscreteTimeCounter("u2");
        DiscreteTimeCounter u3 = new DiscreteTimeCounter("u3");
        DiscreteTimeCounter e1 = new DiscreteTimeCounter("e1");
        DiscreteTimeCounter hc2 = new DiscreteTimeCounter("h2");
        DiscreteTimeCounter hc3 = new DiscreteTimeCounter("h3");

        u1.reset();
        u2.reset();
        u3.reset();
        e1.reset();
        hc2.reset();
        hc3.reset();

        for (int i = 0; i < 1000000; i++) {
            u1.count(Uniform1.getRV());
            u2.count(Uniform2.getRV());
            u3.count(Uniform3.getRV());
            e1.count(Exponential1.getRV());
            hc2.count(h2.getRV());
            hc3.count(h3.getRV());
        }

        u1.report();
        u2.report();
        u3.report();
        e1.report();
        hc2.report();
        hc3.report();
    }
}
